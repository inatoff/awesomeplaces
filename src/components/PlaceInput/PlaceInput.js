import React, { Component } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';

import DefaultInput from '../UI/DefaultInput/DefaultInput';

const placeInput = props => (
  <DefaultInput
    value={props.placeName}
    onChangeText={props.onChangeText}
    {...props}
  />
);

export default PlaceInput;
