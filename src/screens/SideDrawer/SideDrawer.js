import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class SideDrawer extends Component {
  render() {
    return (
      <View
        style={[
          styles.container,
          { width: Dimensions.get('window').width * 0.8 },
        ]}
      >
        <TouchableOpacity>
          <View style={styles.drawerItem}>
            <Icon
              name={Platform.OS === 'android' ? 'md-log-out' : 'ios-log-out'}
              size={30}
              color="#888"
              style={styles.drawerItemIcon}
            />
            <Text>Log out</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.drawerItem}>
            <Icon
              name={
                Platform.OS === 'android'
                  ? 'md-information-circle'
                  : 'ios-information-circle-outline'
              }
              size={30}
              color="#888"
              style={styles.drawerItemIcon}
            />
            <Text>About program</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 22,
    backgroundColor: 'white',
    flex: 1,
  },
  drawerItem: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#eee',
    marginBottom: 5,
  },
  drawerItemIcon: {
    marginRight: 10,
  },
});

export default SideDrawer;
